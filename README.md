# Wallpaper Cycler
### Introduction
Wallpaper cycler is a python utility used to change windows background image based on the current time of the day.

### Usage:
    1. Using sysinternals Bginfo.exe, create bgi files for the different hours of the day.
    2. Create a folder containing the bgi files, named 'hour.bgi'.
        Example -> A folder looking like this: ['6.bgi', '12.bgi', '18.bgi', '0.bgi']
                   Will display the following:
                        0.bgi -> midnight to 05:59
                        6.bgi -> 06:00 to 11:59
                        12.bgi -> 12:00 to 17:59
                        18.bgi -> 18.00 to 23:59
        Make sure to name the files as a single digit number! ('6.bgi' and not '06.bgi')
    3. Edit settings.py to match the directory you have created and the path to Bginfo.exe

Enjoy this utility! 
I recommend adding it as a scheduled task to take this script to its full potential.

`OG7`