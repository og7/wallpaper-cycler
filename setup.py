import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="wallpaper-cycler-OG7",
    version="0.0.1",
    author="og7",
    description="A python utility to change wallpaper by different times of the day.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/og7/wallpaper-cycler",
    packages=setuptools.find_packages(),
    python_requires='>=3.6',
    entry_points={
        'console_scripts': ['wallpaper_cycler=wallpaper_cycler.main:main']
    }
)
