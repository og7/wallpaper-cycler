import os


class BgiDir:
    BGI_EXTENSION = '.bgi'

    def __init__(self, path: str):
        self._path: str = path
        self.files: list[str] = [filename for filename in os.listdir(path)
                                 if filename.endswith(self.BGI_EXTENSION)]
