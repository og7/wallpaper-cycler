from wallpaper_cycler.wallpaper_cycler import WallpaperCycler
from wallpaper_cycler.settings import WALLPAPERS_PATH


def main():
    cycler = WallpaperCycler(WALLPAPERS_PATH)
    cycler.apply_wallpaper()


if __name__ == '__main__':
    main()
