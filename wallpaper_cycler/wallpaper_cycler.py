import os.path

from wallpaper_cycler.wallpaper_selector import WallpaperSelector
from wallpaper_cycler.wallpaper_modifier import WallpaperModifier


class WallpaperCycler:
    def __init__(self, wallpapers_path: str):
        self._wallpapers_path: str = wallpapers_path
        self._selector = WallpaperSelector(self._wallpapers_path)

    def _get_current_wallpaper(self) -> str:
        wallpaper_name: str = self._selector.get_current_wallpaper()
        return os.path.join(self._wallpapers_path, wallpaper_name)

    def apply_wallpaper(self):
        wallpaper_path: str = self._get_current_wallpaper()
        WallpaperModifier.change_wallpaper(wallpaper_path)
