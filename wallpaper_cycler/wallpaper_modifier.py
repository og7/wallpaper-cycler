import os

from wallpaper_cycler.settings import BGINFO_PATH


class WallpaperModifier:
    WALLPAPER_MODIFICATION_COMMAND = '{bginfo} "{wallpaper_path}" /timer 0 /silent'

    @staticmethod
    def _create_wallpaper_modification_command(wallpaper_path: str) -> str:
        return WallpaperModifier.WALLPAPER_MODIFICATION_COMMAND.format(
            bginfo=BGINFO_PATH,
            wallpaper_path=wallpaper_path
        )

    @staticmethod
    def change_wallpaper(wallpaper_path: str):
        wallpaper_modification_command: str = \
            WallpaperModifier._create_wallpaper_modification_command(wallpaper_path)
        os.system(wallpaper_modification_command)
