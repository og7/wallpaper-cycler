from datetime import datetime

from wallpaper_cycler.bgi_dir import BgiDir


class WallpaperSelector:
    def __init__(self, bgi_path: str):
        self._bgi_dir = BgiDir(bgi_path)

    def get_current_wallpaper(self) -> str:
        current_hour: int = datetime.now().hour
        bgi_files: list[str] = self._bgi_dir.files[:]
        current_hour_bgi: str = '{}.bgi'.format(current_hour)

        if current_hour_bgi in bgi_files:
            return current_hour_bgi

        # When not able to find a file that matches the current hour, take the one before.
        bgi_files.append(current_hour_bgi)
        bgi_files.sort()
        current_hour_index: int = bgi_files.index(current_hour_bgi)
        return bgi_files[current_hour_index - 1]

